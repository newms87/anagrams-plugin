Note: Simply put, I have run out of time to complete everything I wanted to for this project, so I am submitting it with most of it done. 
This also means I was not able to complete nearly as much documentation as I would have liked. So please ask dan@amploweb.com for more info if you need it!


Questions (I have yet to ask):
* Does 'A' count as an anagram for 'a'?



User Interface:
/words/ui - visit this page in your browser. It is a graphical User Interface to interact with some of the GET / POST / DELETE endpoints in the API.

Endpoints:
GET /words/counts - returns the total count of words in the corpus w/ max/min/average/median values
GET /anagrams/most - returns the anagram groups w/ the largest size
GET /anagrams/size/{size} returns the anagram groups w/ size >= {size}
DELETE /anagrams/:word.json - will delete the word and all of its anagrams

Edge Cases:
* 1 letter words cannot have anagrams (slight optimizations can be made here)
* The words that are both proper nouns and not proper nouns can be the same spelling (presumably not an anagram of itself tho?) - eg: Piper and piper


Tradeoffs:
* storing letter counts in the word table (eg: 4 a's, 2'bs, 0'cs, etc)
  - decided against this because it would make the table a lot more lengthy and decidedly an unncessary approach. Would make finding anagrams quicker, but this only has to be done 1 time upon saving words to the corpus.

* Grabbing all possible anagrams for a word (eg: word length, or word length + letters matching, etc). and using PHP to decide if it is an anagram
   - Decided to use MySQL to determine the anagram as this was likely faster instead of returning a large list of data. 
   - Only needed 1 record returned as the record would have the anagram group associated.

* Creating a new table for anagram_group or using COUNT(*) to find # of matches in anagram group
   - Decided to create a new table. This will add easy extensibility for the problem and will be a little bit faster, although a little extra effort to implement will be rqeuired and maintain a count field.