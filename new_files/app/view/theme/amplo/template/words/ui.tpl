<?= $is_ajax ? '' : call('header'); ?>

<div class="row words-main">
	<div class="row words-header">
		<h1>{{Words User Interface}}</h1>
	</div>

	<div class="row words-content">
		<div class="words-slider-ctrl">
			<div class="col xs-12 md-10 lg-8 xl-6">
				<a class="col xs-4 words-slider-btn is-active" data-slide="words-post">{{POST}}</a>
				<a class="col xs-4 words-slider-btn" data-slide="words-get">{{GET}}</a>
				<a class="col xs-4 words-slider-btn" data-slide="words-delete">{{DELETE}}</a>
			</div>
		</div>

		<div class="row words-slider">
			<section class="col xs-12 words-post">
				<div class="wrap">
					<div class="col xs-12 md-10 lg-8 xl-6">
						<div class="section-header">
							<h2>{{POST}}</h2>
							<div class="help padding-vertical">{{Create new words!
								<br>Enter a comma or space separated list of words you want to add to the corpus.}}
							</div>
						</div>
						<div class="section-content">
							<form action="<?= site_url('words'); ?>" method="POST">
								<textarea name="words" placeholder="{{(eg: hello, world, cornhole)}}"></textarea>

								<button data-loading="{{Submitting...}}">{{Submit POST}}</button>
							</form>
						</div>
					</div>
				</div>
			</section>

			<section class="col xs-12 words-get">
				<div class="wrap">
					<div class="col xs-12 md-10 lg-8 xl-6">
						<div class="section-header">
							<h2>{{GET}}</h2>
							<div class="help padding-vertical">
								{{Find the anagrams for any word!}}
							</div>
						</div>
						<div class="section-content">
							<form action="<?= site_url('anagrams'); ?>" method="GET">
								<div class="row input-row">
									<input type="text" name="word" placeholder="{{(Enter any word)}}"/>
								</div>

								<div class="row options-row padding-top ">
									<div class="col auto">
										<input type="text" name="limit" placeholder="{{No Limit}}"/>
										<label class="checkbox">
											<input type="checkbox" name="filter[is_proper]" value="0">
											<div class="label">{{Hide Proper Nouns}}</div>
										</label>
									</div>
								</div>

								<div class="row submit-row padding-top">
									<button data-loading="{{Submitting...}}">{{Submit GET}}</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>

			<section class="col xs-12 words-delete">
				<div class="wrap">
					<div class="col xs-12 md-10 lg-8 xl-6">
						<div class="section-header">
							<h2>{{DELETE}}</h2>
							<div class="help padding-vertical">
								{{Delete words from the corpus.}}
							</div>
						</div>
						<div class="section-content">
							<form action="<?= site_url('words'); ?>" method="DELETE">
								<div class="row input-row">
									<input type="text" name="word" placeholder="{{(Enter a word to remove)}}"/>
								</div>

								<div class="row submit-row padding-top">
									<button data-loading="{{Submitting...}}">{{Submit DELETE}}</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="row words-response hidden">
			<h3 class="padding-vertical">{{Showing Anagrams for}} <span class="word"></span></h3>
			<div class="padding-top words-response-anagrams"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var $wordsSlider = $('.words-slider'), $wordsResponse = $('.words-response');

	$('.words-slider-btn').click(function() {
		var $this = $(this);
		var left = 100 * $wordsSlider.find('section.' + $this.attr('data-slide')).index();
		$wordsSlider.css('left', -left + 'vw');

		$('.words-slider-btn').removeClass('is-active');
		$this.addClass('is-active');
	})

	$('.words-content form').submit(function() {
		var $form = $(this), formData;

		var url = $form.attr('action');

		$('body').show_msg('clear');
		$form.find('button').loading();
		$wordsResponse.addClass('hidden');

		switch ($form.attr('method')) {
			case 'POST':
				formData = JSON.stringify({words: $form.find('[name=words]').val().split(/[,\s]+/)});
				break;

			case 'GET':
				formData = $form.find('.options-row [name]').serialize()

			case 'DELETE':
				var word = $form.find('[name=word]').val();

				if (!word.match(/^[a-z]+$/i)) {
					$form.show_msg('error', "Invalid word entered. Only alphabetical characters allowed.");
					setTimeout(function() {$form.find('button').loading('stop');}, 500);
					return false;
				}
				url += '/' + word;
				break;
		}

		$.ajax({
			type:        $form.attr('method'),
			url:         url,
			data:        formData,
			success:     function() {},
			dataType:    "json",
			contentType: "application/json",
			complete:    function(xhr, status) {
				var response = xhr.responseJSON || {};

				console.log(response, xhr, status);

				$form.find('button').loading('stop');

				if (xhr.status >= 400) {
					if (!response.error) {
						$form.show_msg('error', "{{The request returned an error: }}" + (xhr.statusText || "No Response"));
					}
				} else if (xhr.status >= 200 && xhr.status < 300) {
					$form.show_msg('success', "{{The request was successful: }}" + xhr.statusText);
				} else {
					$form.show_msg('error', "{{There was a problem completing your request. Please try again later.}}");
				}

				if (response.error) {
					$form.show_msg('error', response.error, {clear: false});
				}

				if (response.anagrams) {
					$wordsResponse.find('.word').html(response.word);
					$wordsResponse.find('.words-response-anagrams').html(response.anagrams.join(', '));
					$wordsResponse.removeClass('hidden');
				}
			}
		});

		return false;
	})
</script>

<?= $is_ajax ? '' : call('footer'); ?>
