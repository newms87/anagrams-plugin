<?php

/**
 * @author  Daniel Newman
 * @date    6/5/2016
 * @package Anagram
 * @link    http://amplomvc.com/anagram
 *
 */
class App_Model_Word extends App_Model_Table
{
	protected $table = 'word', $primary_key = 'word_id';

	public function exists($word)
	{
		return $this->queryVar("SELECT word_id FROM {$this->t[$this->table]} WHERE BINARY word = '" . $this->escape($word) . "'");
	}

	public function save($word_id, $word)
	{
		if ($word_id) {
			$this->error['word_id'] = _l("Updates to words in the dictionary are not allowed.");

			return false;
		}

		if (isset($word['word'])) {
			$word['word'] = trim($word['word']);

			if (!validate('text', $word['word'], 1, 45)) {
				$this->error['word'] = _l("The word length must be between 1 and 45 characters.");
			} elseif (preg_match("/[^a-z]/i", $word['word'])) {
				$this->error['word'] = _l("The word can only contain alphabetic characters A-Z");
			} elseif ($this->exists($word['word'])) {
				$this->error['duplicate'] = _l("The word %s already exists in the dictionary.", $word['word']);
			}
		} else {
			$this->error['word'] = _l("Word is required to create a new dictionary word");
		}

		if ($this->error) {
			return false;
		}

		$word['length']    = strlen($word['word']);
		$word['is_proper'] = ctype_upper($word['word'][0]);

		if ($anagram = $this->findFirstAnagram($word['word'])) {
			if ($anagram['anagram_group_id']) {
				//Add the new word to the existing anagram group and increment the count of words in the group
				$word['anagram_group_id'] = $anagram['anagram_group_id'];
				$this->query("UPDATE {$this->t['anagram_group']} SET count = count + 1 WHERE anagram_group_id = {$anagram['anagram_group_id']}");
			} else {
				//If the anagram group does not exist yet for this word, create it
				$anagram_group = array(
					'word'   => $anagram['word'],
					'length' => $anagram['length'],
					'count'  => 2,
				);

				if ($anagram_group_id = $this->insert('anagram_group', $anagram_group)) {
					//Update the new word and the original word to both be included in the new anagram group
					$this->update('word', array('anagram_group_id' => $anagram_group_id), $anagram['word_id']);
					$word['anagram_group_id'] = $anagram_group_id;
				}
			}
		}

		return parent::save($word_id, $word);
	}

	public function remove($word_id)
	{
		//Decrement the count in the associated anagram group (if it exists)
		if ($anagram_group_id = $this->getField($word_id, 'anagram_group_id')) {
			if ($anagram_group = $this->queryRow("SELECT * FROM {$this->t['anagram_group']} WHERE anagram_group_id = " . (int)$anagram_group_id)) {
				//If there are more than 2, decrement the count. If there are 2 or less, we should remove this group from the system as there are no anagrams left for this letter combination
				if ($anagram_group['count'] > 2) {
					$this->update('anagram_group', array('count' => $anagram_group['count'] - 1), $anagram_group_id);
				} else {
					//Delete the group, and set the anagram group to 0 for the existing word in the group.
					$this->delete('anagram_group', $anagram_group_id);
					$this->update('word', array('anagram_group_id' => 0), array('anagram_group_id' => $anagram_group_id));
				}
			}
		}

		return parent::remove($word_id);
	}

	public function getAnagrams($word_id, $filter = array(), $limit = null)
	{
		$anagram_group_id = $this->getField($word_id, 'anagram_group_id');

		if ($anagram_group_id) {
			$limit = abs((int)$limit);
			$where = $filter ? $this->extractWhere($this->table, $filter) : false;

			return $this->queryColumn("SELECT word FROM {$this->t['word']} WHERE anagram_group_id = $anagram_group_id AND word_id != " . (int)$word_id . ($where ? ' AND ' . $where : '') . ($limit ? " LIMIT $limit" : ''));
		} else {
			return array();
		}
	}

	public function getAnagramGroupWords($anagram_group_id)
	{
		return $this->queryColumn("SELECT word FROM {$this->t['word']} WHERE anagram_group_id = " . (int)$anagram_group_id);
	}

	/**
	 * Looks for an existing anagram of this word. If found, returns the existing group
	 *
	 * @param $word_id
	 */
	public function findFirstAnagram($word)
	{
		$word   = strtolower(trim($word));
		$length = strlen($word);

		$filter = array(
			'length' => $length,
			'#word'  => "`word` != '" . $this->escape($word) . "'",
		);

		$letters = str_split($word, 1);

		$letter_count = array();

		foreach ($letters as $letter) {
			if (isset($letter_count[$letter])) {
				$letter_count[$letter] += 1;
			} else {
				$letter_count[$letter] = 1;
			}
		}

		foreach ($letter_count as $letter => $count) {
			$filter['#' . $letter] = "CHAR_LENGTH(REPLACE(LOWER(`word`), '$letter', '')) = " . ($length - $count);
		}

		return $this->findRecord($filter, '*');
	}

	public function getMostAnagrams()
	{
		$size = $this->queryVar("SELECT MAX(count) FROM {$this->t['anagram_group']}");

		return $this->getAnagramGroupsBySize($size);
	}

	public function getAnagramGroupsBySize($size)
	{
		$groups = $this->queryRows("SELECT * FROM {$this->t['anagram_group']} WHERE `count` >= " . (int)$size, 'anagram_group_id');

		foreach ($groups as $group_id => &$group) {
			$group['words'] = $this->getAnagramGroupWords($group_id);
		}
		unset($group);

		return $groups;
	}

	public function getWordCounts()
	{
		$counts = $this->queryRow("SELECT COUNT(*) as total, MIN(length) as min, MAX(length) as max, AVG(length) as average FROM {$this->t[$this->table]}");

		$half = floor($counts['total'] / 2);

		$counts['median'] = $this->queryVar("SELECT length FROM {$this->t[$this->table]} ORDER BY length ASC LIMIT $half, 1");

		return $counts;
	}

	public function removeAnagrams($word_id)
	{
		$anagram_group_id = $this->getField($word_id, 'anagram_group_id');

		if ($anagram_group_id) {
			$this->delete('word', array('anagram_group_id' => $anagram_group_id));
			$this->delete('anagram_group', $anagram_group_id);
		} else {
			$this->remove($word_id);
		}

		return true;
	}
}
