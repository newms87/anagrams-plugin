<?php

/**
 * @author  Daniel Newman
 * @date    6/5/2016
 * @package Anagram
 * @link    http://amplomvc.com/anagram
 *
 */
class App_Controller_Anagrams extends App_Controller_Restful
{
	protected $model = 'App_Model_Word';

	public function __construct()
	{
		define('API_REQUIRE_AUTH', false);

		parent::__construct();

		if (!empty($this->args[0])) {
			//XXX: this is a hack to workaround a caveat in AmploMVC that will automatically call strtolower on the URI path (thus the parameters are not case sensitive).
			$this->args[0] = pathinfo(preg_replace("/\\?.*/", '', $_SERVER['REQUEST_URI']), PATHINFO_FILENAME);
		}
	}

	protected function read()
	{
		if (!empty($this->args[0])) {
			//Translate the word into the word_id so the Restful Controller can properly handle the request
			$this->id = $this->Model_Word->exists($this->args[0]);

			if ($this->id) {
				$response = array(
					'word'     => $this->args[0],
					'anagrams' => $this->Model_Word->getAnagrams($this->id, _request('filter'), _request('limit')),
				);

				output_restful(200, 'OK', $response);
			} else {
				output_restful(404, 'Not Found');
			}
		} else {
			//Let the parent Restful Controller handle the read all response
			return parent::read();
		}
	}

	protected function delete()
	{
		if (!empty($this->args[0])) {
			$this->id = $this->Model_Word->exists($this->args[0]);

			$this->Model_Word->removeAnagrams($this->id);

			output_restful(204, 'No Content');
		}
	}

	public function most()
	{
		$response = $this->Model_Word->getMostAnagrams();

		output_restful(200, 'OK', $response);
	}

	public function size()
	{
		$size = _request('size') ?: $this->id;
		$response = $this->Model_Word->getAnagramGroupsBySize($size);

		output_restful(200, 'OK', $response);
	}


}
