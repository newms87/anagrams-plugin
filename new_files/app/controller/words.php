<?php

/**
 * @author  Daniel Newman
 * @date    6/5/2016
 * @package Anagram
 * @link    http://amplomvc.com/anagram
 *
 */
class App_Controller_Words extends App_Controller_Restful
{
	protected $model = 'App_Model_Word';

	public function __construct()
	{
		define('API_REQUIRE_AUTH', false);

		parent::__construct();

		if (!empty($this->args[0])) {
			//XXX: this is a hack to workaround a caveat in AmploMVC that will automatically call strtolower on the URI path (thus the parameters are not case sensitive).
			$this->args[0] = pathinfo(preg_replace("/\\?.*/", '', $_SERVER['REQUEST_URI']), PATHINFO_FILENAME);
		}
	}

	public function ui()
	{
		//Page Head
		set_page_info('title', _l("Anagrams"));

		set_page_meta('description', _l("Anagrams are a word, phrase, or name formed by rearranging the letters of another, such as cinema, formed from iceman."));

		//Render
		output($this->render('words/ui'));
	}

	protected function create($id = null)
	{
		//Translate the data so the Restful Controller can understand how to create each record
		if (is_array($this->data)) {
			$data = array();

			foreach ($this->data['words'] as $d) {
				$data[] = array(
					'word' => $d
				);
			}

			$this->data = $data;
		}

		return parent::create($id);
	}

	protected function delete()
	{
		if (!empty($this->args[0])) {
			//Translate the word into the word_id so the Restful Controller can properly handle the request
			$this->id = $this->Model_Word->exists($this->args[0]);

			//The Restful Controller would normally delete all the records when $this->id is not set,
			//but we want to return a 404 NOT FOUND instead
			if (!$this->id) {
				return output_restful(404, "Not Found");
			}
		}

		//The Restful Controller can handle the response from here
		return parent::delete();
	}

	public function counts()
	{
		$response = $this->Model_Word->getWordCounts();

		output_restful(200, 'OK', $response);
	}
}
