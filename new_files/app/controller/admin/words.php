<?php

/**
 * @author  Daniel Newman
 * @date    6/5/2016
 * @package Anagram
 * @link    http://amplomvc.com/plugins/anagram
 *
 */
class App_Controller_Admin_Words extends App_Controller_Table
{
	protected $model = array(
		'title' => 'Word',
		'class' => 'App_Model_Word',
		'path'  => 'admin/words',
		'label' => 'word',
		'value' => 'word_id',
	);
}
