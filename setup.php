<?php

/**
 * The Anagram Plugin
 *
 * Version: 0.1
 * Name: anagram
 * Title: Anagram Plugin
 * Description: Anagrams are a word, phrase, or name formed by rearranging the letters of another, such as cinema, formed from iceman.
 * Author: Daniel Newman
 * Date: 6/5/2016
 * Link: http://www.amplomvc.com/plugins/anagram
 *
 * @package Anagram Plugin
 */
class Plugin_Anagram_Setup extends Plugin_Setup
{
	static $admin_nav = array(
		'words' => array(
			'display_name' => "Words",
			'path'         => 'admin/words',
			'sort_order'   => 15,
		)
	);

	public function install()
	{
		//Set timeout to 5 minutes as this may take a little while to write all words to the DB
		set_time_limit(3600);

		$this->Model_Navigation->saveGroupLinks('admin', self::$admin_nav);

		$this->db->createTable('word', <<<SQL
  `word_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `word` VARCHAR(45) NOT NULL,
  `length` INT UNSIGNED NOT NULL,
  `is_proper` TINYINT(3) UNSIGNED NOT NULL,
  `anagram_group_id` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`word_id`),
  INDEX `WORD` (`word` ASC),
  INDEX `LENGTH` (`length` ASC),
  INDEX `GROUP` (`anagram_group_id` ASC)
SQL
		);

		$this->db->createTable('anagram_group', <<<SQL
  `anagram_group_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `word` VARCHAR(45) NOT NULL,
  `length` INT UNSIGNED NOT NULL,
  `count` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`anagram_group_id`)
SQL
		);

		//Add All words in English Dictionary
		$handle = @fopen(DIR_PLUGIN . "anagram/dictionary.txt", "r");

		if ($handle) {
			while (($word = trim(fgets($handle, 4096))) !== false) {
				$this->Model_Word->save(null, array('word' => $word));

				//Clears any errors (we do not show errors here as they are likely duplicate word errors in case of previous install)
				$this->Model_Word->clearErrors();
			}

			fclose($handle);
		} else {
			message('error', _l("Unable to read the Anagram dictionary.txt file!"));
		}
	}

	public function uninstall($keep_data = false)
	{
		$this->Model_Navigation->removeGroupLinks('admin', self::$admin_nav);

		if (!$keep_data) {
			$this->db->dropTable('word');
			$this->db->dropTable('anagram_group');
		}
	}
}
